const mongoose = require('mongoose')
const config = require('config');
const db = mongoose.connect('mongodb://' + config.get('mongodb.address') + '/' + config.get('mongodb.dbname'), { useNewUrlParser: true, useUnifiedTopology: true });

module.exports = db