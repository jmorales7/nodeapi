const logger = require('../utils/logger');
const express = require('express');
const router = express.Router();
const storeController = require('../controllers/store');

router.get('/',storeController.index);
router.post('/',storeController.store);


module.exports = router;
