const request = require( 'supertest' );
const storeModel = require( '../../models/user' );
const app = require( '../../app' );
const dbMongo = require('../../db/database')

describe( 'Tests for store endpoints', function () {
    let token;

    beforeEach( async () => {
        await storeModel.remove({});
        token = 'Basic dGVzdEBrb2liYW54LmNvbTphZG1pbg==';
    });
    it( 'should return a success message creating one store', () => {
        const storeRequest = {
            name: "Esat es una prueba jest",
            cuit:"Este es mi cuit",
            concepts: 
            [
                "prueba_1",
                "prueba_2"
            ],
            currentBalance: 30,
            active: true,
            lastSale:"2022-07-25"
        }
        request( app )
        .post('/api/stores')
        .set('Authorization', token )
        .send( storeRequest )
        .end( ( err, res ) => {
            
            expect( res.body ).toHaveProperty( 'message' );
            expect(res.statusCode).toEqual(200)
        } )
    });
    it( 'should return a failed message without required fiedl', () => {
        const storeRequest = {
            cuit:"Este es mi cuit",
            concepts: 
            [
                "prueba_1",
                "prueba_2"
            ],
            currentBalance: 30,
            active: true,
            lastSale:"2022-07-25"
        }
        request( app )
        .post('/api/stores')
        .set({ 'authorization': token })
        .send( storeRequest )
        .end( ( err, res ) => {
            expect( res.body ).toHaveProperty( 'message' );
        } )
    });
    afterEach( async ( ) => {
        await storeModel.remove({});
    })
});