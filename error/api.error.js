class ApiError {
    constructor(code, message) {
      this.code = code;
      this.message = message;
    }
    
    static successRequest(msg) {
        return new ApiError(200, msg);
      }
    static badRequest(msg) {
      return new ApiError(401, msg);
    }
  
    static internal(msg) {
      return new ApiError(500, msg);
    }
  }
  
  module.exports = ApiError;